package com.homework.constants;

public class AddressConstants {
    public static final String LONDON = "London";

    public static final String TOKYO = "Tokyo";

    public static final String PARIS = "Paris";
}
