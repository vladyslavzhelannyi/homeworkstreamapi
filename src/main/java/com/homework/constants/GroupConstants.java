package com.homework.constants;

public class GroupConstants {
    public static final String TECHNICAL_GROUP = "technical group";

    public static final String SUPPORT_GROUP = "support group";

    public static final String SUPPLY_GROUP = "supply group";
}
