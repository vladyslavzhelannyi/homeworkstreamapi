package com.homework.constants;

public class CourseConstants {
    public static final String FIRST_COURSE = "first course";

    public static final String SECOND_COURSE = "second course";

    public static final String THIRD_COURSE = "third course";

    public static final String FOURTH_COURSE = "fourth course";
}
