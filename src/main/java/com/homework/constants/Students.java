package com.homework.constants;

import com.homework.model.Student;
import static com.homework.constants.AddressConstants.*;
import static com.homework.constants.CourseConstants.*;
import static com.homework.constants.FacultyConstants.*;
import static com.homework.constants.GroupConstants.*;

public class Students {
    public static final Student STUDENT1 = new Student(1, "fn1", "ln1", 1998, LONDON,
            "0951111111", IT_FACULTY, SECOND_COURSE, SUPPORT_GROUP);
    public static final Student STUDENT2 = new Student(2, "fn2", "ln2", 1997, TOKYO,
            "0952222222", ECONOMIC_FACULTY, FIRST_COURSE, SUPPLY_GROUP);
    public static final Student STUDENT3 = new Student(3, "fn3", "ln3", 1996, LONDON,
            "0953333333", IT_FACULTY, THIRD_COURSE, SUPPORT_GROUP);
    public static final Student STUDENT4 = new Student(4, "fn4", "ln4", 1993, TOKYO,
            "0954444444", IT_FACULTY, FOURTH_COURSE, TECHNICAL_GROUP);
    public static final Student STUDENT5 = new Student(5, "fn5", "ln5", 2003, PARIS,
            "0955555555", CHEMISTRY_FACULTY, THIRD_COURSE, TECHNICAL_GROUP);
    public static final Student STUDENT6 = new Student(6, "fn6", "ln6", 1994, PARIS,
            "0956666666", CHEMISTRY_FACULTY, SECOND_COURSE, SUPPLY_GROUP);
    public static final Student STUDENT7 = new Student(7, "fn7", "ln7", 1992, PARIS,
            "0957777777", IT_FACULTY, FIRST_COURSE, SUPPORT_GROUP);
    public static final Student STUDENT8 = new Student(8, "fn8", "ln8", 2002, LONDON,
            "0958888888", CHEMISTRY_FACULTY, SECOND_COURSE, SUPPLY_GROUP);
    public static final Student STUDENT9 = new Student(9, "fn9", "ln9", 1996, TOKYO,
            "0959999999", ECONOMIC_FACULTY, FOURTH_COURSE, TECHNICAL_GROUP);
    public static final Student STUDENT10 = new Student(10, "fn10", "ln10", 1995, LONDON,
            "0951010101", ECONOMIC_FACULTY, THIRD_COURSE, SUPPORT_GROUP);
    public static final Student STUDENT11 = new Student(11, "fn11", "ln11", 2004, PARIS,
            "0951111111", CHEMISTRY_FACULTY, FIRST_COURSE, SUPPLY_GROUP);
    public static final Student STUDENT12 = new Student(12, "fn12", "ln12", 1998, TOKYO,
            "0951212121", ECONOMIC_FACULTY, SECOND_COURSE, TECHNICAL_GROUP);
}
