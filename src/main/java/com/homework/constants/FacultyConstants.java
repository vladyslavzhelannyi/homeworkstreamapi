package com.homework.constants;

public class FacultyConstants {
    public static final String ECONOMIC_FACULTY = "economic faculty";

    public static final String IT_FACULTY = "it faculty";

    public static final String CHEMISTRY_FACULTY = "chemistry faculty";
}
