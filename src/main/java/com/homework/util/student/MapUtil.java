package com.homework.util.student;

import com.homework.model.Student;

import java.util.List;
import java.util.stream.Collectors;

public class MapUtil {
    public List<Student> transferToFaculty(List<Student> students, String faculty) {
        List<Student> transferStudents = students.stream()
                .map(student -> new Student(student.getId(), student.getFirstName(), student.getLastName(),
                        student.getYearOfBirth(), student.getAddress(), student.getTelephone(), faculty,
                        student.getCourse(), student.getGroup()))
                .collect(Collectors.toList());
        return transferStudents;
    }

    public List<Student> transferToGroup(List<Student> students, String group) {
        List<Student> transferStudents = students.stream()
                .map(student -> new Student(student.getId(), student.getFirstName(), student.getLastName(),
                        student.getYearOfBirth(), student.getAddress(), student.getTelephone(), student.getFaculty(),
                        student.getCourse(), group))
                .collect(Collectors.toList());
        return transferStudents;
    }
}
