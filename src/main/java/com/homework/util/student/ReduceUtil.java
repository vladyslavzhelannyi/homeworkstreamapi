package com.homework.util.student;

import com.homework.model.Student;

import java.util.List;

public class ReduceUtil {
    public String getStudentsInfo(List<Student> students, int studentsAmount) {
        String studentsInfo = students.stream()
                .limit(studentsAmount)
                .map(student -> student.getLastName()
                        .concat(", ")
                        .concat(student.getFirstName())
                        .concat(" - ")
                        .concat(student.getFaculty())
                        .concat(", ")
                        .concat(student.getGroup()))
                .reduce("",
                        (partialString, studentString) -> partialString.concat("\n").concat(studentString));
        return studentsInfo;
    }
}
