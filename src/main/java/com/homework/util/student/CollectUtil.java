package com.homework.util.student;

import com.homework.model.Student;

import java.util.List;
import java.util.stream.Collectors;

public class CollectUtil {
    public List<Student> getListFacultyStudents(List<Student> students, String faculty) {
        List<Student> facultyStudents = students.stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .collect(Collectors.toList());
        return facultyStudents;
    }

    public List<Student> getListCourseStudents(List<Student> students, String course) {
        List<Student> courseStudents = students.stream()
                .filter(student -> student.getCourse().equals(course))
                .collect(Collectors.toList());
        return courseStudents;
    }

    public List<Student> getListOfStudentsBornAfter(List<Student> students, int year) {
        List<Student> bornAfterStudents = students.stream()
                .filter(student -> student.getYearOfBirth() > year)
                .collect(Collectors.toList());
        return bornAfterStudents;
    }

    public Student getStudentBornAfter(List<Student> students, int year) {
        Student bornAfterStudent = students.stream()
                .filter(student -> student.getYearOfBirth() > year)
                .findAny()
                .get();
        return bornAfterStudent;
    }

    public List<String> getGroupList(List<Student> students) {
        List<String> groupList = students.stream()
                .map(student -> student.getLastName().concat(", ").concat(student.getFirstName()))
                .collect(Collectors.toList());
        return groupList;
    }
}
