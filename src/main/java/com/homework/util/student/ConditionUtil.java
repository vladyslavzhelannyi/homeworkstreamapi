package com.homework.util.student;

import com.homework.model.Student;

import java.util.List;

public class ConditionUtil {
    public boolean ifAllOnFaculty(List<Student> students, String faculty) {
        boolean areAllOnFaculty = students.stream()
                .allMatch(student -> student.getFaculty().equals(faculty));
        return areAllOnFaculty;
    }

    public boolean ifAnyoneOnFaculty(List<Student> students, String faculty) {
        boolean isAnyoneOnFaculty = students.stream()
                .anyMatch(student -> student.getFaculty().equals(faculty));
        return isAnyoneOnFaculty;
    }

    public boolean ifAllInGroup(List<Student> students, String group) {
        boolean areAllInGroup = students.stream()
                .allMatch(student -> student.getGroup().equals(group));
        return areAllInGroup;
    }

    public boolean ifAnyoneInGroup(List<Student> students, String group) {
        boolean isAnyoneInGroup = students.stream()
                .anyMatch(student -> student.getGroup().equals(group));
        return isAnyoneInGroup;
    }
}
