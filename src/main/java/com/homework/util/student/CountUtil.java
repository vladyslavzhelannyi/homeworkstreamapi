package com.homework.util.student;

import com.homework.model.Student;

import java.util.List;

public class CountUtil {
    public int getFacultyStudentsNumber(List<Student> students, String faculty) {
        int facultyStudentsNumber = (int) students.stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .count();
        return facultyStudentsNumber;
    }
}
