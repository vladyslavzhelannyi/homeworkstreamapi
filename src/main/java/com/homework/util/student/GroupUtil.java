package com.homework.util.student;

import com.homework.model.Student;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupUtil {
    public Map<String, List<Student>> getFacultyMap(List<Student> students) {
        Map<String, List<Student>> studentsByFaculty = students.stream()
                .collect(Collectors.groupingBy(Student::getFaculty));
        return studentsByFaculty;
    }

    public Map<String, List<Student>> getCourseMap(List<Student> students) {
        Map<String, List<Student>> studentsByCourse = students.stream()
                .collect(Collectors.groupingBy(Student::getCourse));
        return studentsByCourse;
    }

    public Map<String, List<Student>> getGroupMap(List<Student> students) {
        Map<String, List<Student>> studentsByGroup = students.stream()
                .collect(Collectors.groupingBy(Student::getGroup));
        return studentsByGroup;
    }
}
