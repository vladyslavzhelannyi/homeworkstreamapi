package com.homework.util.student;

import com.homework.model.Student;

import java.util.List;

public class FindUtil {
    public int countStudentsOnFaculty(List<Student> students, String faculty) {
        int studentsOnFaculty = (int) students.stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .count();
        return studentsOnFaculty;
    }
}
