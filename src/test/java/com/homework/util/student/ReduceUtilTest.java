package com.homework.util.student;

import com.homework.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static testdata.ReduceUtilTestData.*;

public class ReduceUtilTest {
    private static final ReduceUtil cut = new ReduceUtil();

    static Arguments[] getStudentsInfoTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentsList1, STUDENTS_AMOUNT_1, EXPECTED_1),
                Arguments.arguments(studentsList2, STUDENTS_AMOUNT_2, EXPECTED_2),
        };
    }

    @ParameterizedTest
    @MethodSource("getStudentsInfoTestArgs")
    void getStudentsInfoTest(List<Student> students, int studentsAmount, String expected) {
        String actual = cut.getStudentsInfo(students, studentsAmount);
        Assertions.assertEquals(expected, actual);
    }
}
