package com.homework.util.student;

import com.homework.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static testdata.CollectUtilTestData.*;

public class CollectUtilTest {
    private static final CollectUtil cut = new CollectUtil();

    static Arguments[] getListFacultyStudentsTestArgs() {
        return new Arguments[] {
                Arguments.arguments(students1, FACULTY_1, expected1),
                Arguments.arguments(students2, FACULTY_2, expected2),
        };
    }

    @ParameterizedTest
    @MethodSource("getListFacultyStudentsTestArgs")
    void getListFacultyStudentsTest(List<Student> students, String faculty,  List<Student> expected) {
        List<Student> actual = cut.getListFacultyStudents(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getListCourseStudentsTestArgs() {
        return new Arguments[] {
                Arguments.arguments(students3, COURSE_1, expected3),
                Arguments.arguments(students4, COURSE_2, expected4),
        };
    }

    @ParameterizedTest
    @MethodSource("getListCourseStudentsTestArgs")
    void getListCourseStudentsTest(List<Student> students, String course, List<Student> expected) {
        List<Student> actual = cut.getListCourseStudents(students, course);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getListOfStudentsBornAfterTestArgs() {
        return new Arguments[] {
                Arguments.arguments(students5, YEAR_1, expected5),
                Arguments.arguments(students6, YEAR_2, expected6),
        };
    }

    @ParameterizedTest
    @MethodSource("getListOfStudentsBornAfterTestArgs")
    void getListOfStudentsBornAfterTest(List<Student> students, int year, List<Student> expected) {
        List<Student> actual = cut.getListOfStudentsBornAfter(students, year);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getStudentBornAfterTestArgs() {
        return new Arguments[] {
                Arguments.arguments(students7, YEAR_3, expected7),
                Arguments.arguments(students8, YEAR_4, expected8),
        };
    }

    @ParameterizedTest
    @MethodSource("getStudentBornAfterTestArgs")
    void getStudentBornAfterTest(List<Student> students, int year, Student expected) {
        Student actual = cut.getStudentBornAfter(students, year);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getGroupListTestArgs() {
        return new Arguments[] {
                Arguments.arguments(students9, expected9),
                Arguments.arguments(students10, expected10),
        };
    }

    @ParameterizedTest
    @MethodSource("getGroupListTestArgs")
    void getGroupListTest(List<Student> students, List<String> expected) {
        List<String> actual = cut.getGroupList(students);
        Assertions.assertEquals(expected, actual);
    }
}
