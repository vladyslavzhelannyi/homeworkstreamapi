package com.homework.util.student;

import com.homework.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static com.homework.constants.FacultyConstants.ECONOMIC_FACULTY;
import static com.homework.constants.FacultyConstants.IT_FACULTY;
import static testdata.FindUtilTestData.*;

public class FindUtilTest {
    private static final FindUtil cut = new FindUtil();

    static Arguments[] countStudentsOnFacultyTestArgs() {
        return new Arguments[] {
                Arguments.arguments(STUDENT_LIST_1, ECONOMIC_FACULTY, FACULTY_STUDENTS_NUMBER_1),
                Arguments.arguments(STUDENT_LIST_2, IT_FACULTY, FACULTY_STUDENTS_NUMBER_2),
        };
    }

    @ParameterizedTest
    @MethodSource("countStudentsOnFacultyTestArgs")
    void countStudentsOnFacultyTest(List<Student> studentList, String faculty, int expected) {
        int actual = cut.countStudentsOnFaculty(studentList, faculty);
        Assertions.assertEquals(expected, actual);
    }
}
