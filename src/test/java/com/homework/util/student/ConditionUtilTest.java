package com.homework.util.student;

import com.homework.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static testdata.ConditionalUtilTestData.*;

public class ConditionUtilTest {
    private static final ConditionUtil cut = new ConditionUtil();

    static Arguments[] ifAllOnFacultyTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentList1, FACULTY_1, ALL_ON_FACULTY),
                Arguments.arguments(studentList2, FACULTY_2, NOT_ALL_ON_FACULTY),
        };
    }

    @ParameterizedTest
    @MethodSource("ifAllOnFacultyTestArgs")
    void ifAllOnFacultyTest(List<Student> students, String faculty, boolean expected) {
        boolean actual = cut.ifAllOnFaculty(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] ifAnyoneOnFacultyTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentList3, FACULTY_3, ANYONE_ON_FACULTY),
                Arguments.arguments(studentList4, FACULTY_4, NOT_ANYONE_ON_FACULTY),
        };
    }

    @ParameterizedTest
    @MethodSource("ifAnyoneOnFacultyTestArgs")
    void ifAnyoneOnFacultyTest(List<Student> students, String faculty, boolean expected) {
        boolean actual = cut.ifAnyoneOnFaculty(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] ifAllInGroupTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentList5, GROUP_1, ALL_IN_GROUP),
                Arguments.arguments(studentList6, GROUP_2, NOT_ALL_IN_GROUP),
        };
    }

    @ParameterizedTest
    @MethodSource("ifAllInGroupTestArgs")
    void ifAllInGroupTest(List<Student> students, String group, boolean expected) {
        boolean actual = cut.ifAllInGroup(students, group);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] ifAnyoneInGroupTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentList7, GROUP_3, ANYONE_IN_GROUP),
                Arguments.arguments(studentList8, GROUP_4, NOT_ANYONE_IN_GROUP),
        };
    }

    @ParameterizedTest
    @MethodSource("ifAnyoneInGroupTestArgs")
    void ifAnyoneInGroupTest(List<Student> students, String group, boolean expected) {
        boolean actual = cut.ifAnyoneInGroup(students, group);
        Assertions.assertEquals(expected, actual);
    }
}
