package com.homework.util.student;

import com.homework.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.List;
import static com.homework.constants.FacultyConstants.CHEMISTRY_FACULTY;
import static com.homework.constants.FacultyConstants.IT_FACULTY;
import static testdata.CountUtilTestData.*;

public class CountUtilTest {
    private static final CountUtil cut = new CountUtil();

    static Arguments[] getFacultyStudentsNumberTestArgs() {
        return new Arguments[] {
                Arguments.arguments(STUDENT_LIST_1, IT_FACULTY, FACULTY_STUDENTS_NUMBER_1),
                Arguments.arguments(STUDENT_LIST_2, CHEMISTRY_FACULTY, FACULTY_STUDENTS_NUMBER_2),
        };
    }

    @ParameterizedTest
    @MethodSource("getFacultyStudentsNumberTestArgs")
    void getFacultyStudentsNumberTest(List<Student> studentList, String faculty, int expected) {
        int actual = cut.getFacultyStudentsNumber(studentList, faculty);
        Assertions.assertEquals(expected, actual);
    }
}
