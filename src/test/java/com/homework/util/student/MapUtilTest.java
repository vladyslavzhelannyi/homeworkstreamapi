package com.homework.util.student;

import com.homework.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.List;
import static testdata.MapUtilTestData.*;

public class MapUtilTest {
    private static final MapUtil cut = new MapUtil();

    static Arguments[] transferToFacultyTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentList1, FACULTY_1, expected1),
                Arguments.arguments(studentList2, FACULTY_2, expected2),
        };
    }

    @ParameterizedTest
    @MethodSource("transferToFacultyTestArgs")
    void transferToFacultyTest(List<Student> students, String faculty, List<Student> expected) {
        List<Student> actual = cut.transferToFaculty(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] transferToGroupTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentList3, GROUP_1, expected3),
                Arguments.arguments(studentList4, GROUP_2, expected4),
        };
    }

    @ParameterizedTest
    @MethodSource("transferToGroupTestArgs")
    void transferToGroupTest(List<Student> students, String group, List<Student> expected) {
        List<Student> actual = cut.transferToGroup(students, group);
        Assertions.assertEquals(expected, actual);
    }
}
