package com.homework.util.student;

import com.homework.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Map;

import static testdata.GroupUtilTestData.*;

public class GroupUtilTest {
    private static final GroupUtil cut = new GroupUtil();

    static Arguments[] getFacultyMapTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentList1, expected1),
                Arguments.arguments(studentList2, expected2),
        };
    }

    @ParameterizedTest
    @MethodSource("getFacultyMapTestArgs")
    void getFacultyMapTest(List<Student> students, Map<String, List<Student>> expected) {
        Map<String, List<Student>> actual = cut.getFacultyMap(students);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getCourseMapTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentList3, expected3),
                Arguments.arguments(studentList4, expected4),
        };
    }

    @ParameterizedTest
    @MethodSource("getCourseMapTestArgs")
    void getCourseMapTest(List<Student> students, Map<String, List<Student>> expected) {
        Map<String, List<Student>> actual = cut.getCourseMap(students);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getGroupMapTestArgs() {
        return new Arguments[] {
                Arguments.arguments(studentList5, expected5),
                Arguments.arguments(studentList6, expected6),
        };
    }

    @ParameterizedTest
    @MethodSource("getGroupMapTestArgs")
    void getGroupMapTest(List<Student> students, Map<String, List<Student>> expected) {
        Map<String, List<Student>> actual = cut.getGroupMap(students);
        Assertions.assertEquals(expected, actual);
    }
}
