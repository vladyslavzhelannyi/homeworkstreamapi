package testdata;
import com.homework.model.Student;

import java.util.ArrayList;
import java.util.List;

import static com.homework.constants.Students.*;
public class ReduceUtilTestData {
    public static final List<Student> studentsList1 = new ArrayList<>(List.of(STUDENT1, STUDENT2, STUDENT3, STUDENT4));
    public static final List<Student> studentsList2 = new ArrayList<>(List.of(STUDENT5, STUDENT6, STUDENT7, STUDENT8));
    public static final int STUDENTS_AMOUNT_1 = 5;
    public static final int STUDENTS_AMOUNT_2 = 3;
    public static final String EXPECTED_1 = "\nln1, fn1 - it faculty, support group\n" +
            "ln2, fn2 - economic faculty, supply group\n" +
            "ln3, fn3 - it faculty, support group\n" +
            "ln4, fn4 - it faculty, technical group";
    public static final String EXPECTED_2 = "\nln5, fn5 - chemistry faculty, technical group\n" +
            "ln6, fn6 - chemistry faculty, supply group\n" +
            "ln7, fn7 - it faculty, support group";
}
