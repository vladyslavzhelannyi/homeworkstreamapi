package testdata;
import com.homework.model.Student;

import java.util.ArrayList;
import java.util.List;

import static com.homework.constants.Students.*;

public class FindUtilTestData {
    public static final List<Student> STUDENT_LIST_1 = new ArrayList<>(List.of(STUDENT2, STUDENT7, STUDENT8, STUDENT11));
    public static final List<Student> STUDENT_LIST_2 = new ArrayList<>(List.of(STUDENT1, STUDENT3, STUDENT4, STUDENT7));
    public static final int FACULTY_STUDENTS_NUMBER_1 = 1;
    public static final int FACULTY_STUDENTS_NUMBER_2 = 4;
}
