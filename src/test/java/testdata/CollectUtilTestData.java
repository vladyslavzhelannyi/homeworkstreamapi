package testdata;
import com.homework.model.Student;

import java.util.ArrayList;
import java.util.List;

import static com.homework.constants.CourseConstants.FIRST_COURSE;
import static com.homework.constants.CourseConstants.THIRD_COURSE;
import static com.homework.constants.FacultyConstants.CHEMISTRY_FACULTY;
import static com.homework.constants.FacultyConstants.IT_FACULTY;
import static com.homework.constants.Students.*;
public class CollectUtilTestData {
    public static final List<Student> students1 = new ArrayList<>(List.of(STUDENT1, STUDENT2, STUDENT3));
    public static final List<Student> students2 = new ArrayList<>(List.of(STUDENT4, STUDENT5, STUDENT6));
    public static final List<Student> students3 = new ArrayList<>(List.of(STUDENT7, STUDENT8, STUDENT9));
    public static final List<Student> students4 = new ArrayList<>(List.of(STUDENT3, STUDENT5, STUDENT10));
    public static final List<Student> students5 = new ArrayList<>(List.of(STUDENT5, STUDENT11, STUDENT12));
    public static final List<Student> students6 = new ArrayList<>(List.of(STUDENT6, STUDENT7, STUDENT8));
    public static final List<Student> students7 = new ArrayList<>(List.of(STUDENT1, STUDENT4, STUDENT5));
    public static final List<Student> students8 = new ArrayList<>(List.of(STUDENT6, STUDENT7, STUDENT9));
    public static final List<Student> students9 = new ArrayList<>(List.of(STUDENT3, STUDENT6, STUDENT9));
    public static final List<Student> students10 = new ArrayList<>(List.of(STUDENT4, STUDENT8, STUDENT12));
    public static final String FACULTY_1 = IT_FACULTY;
    public static final String FACULTY_2 = CHEMISTRY_FACULTY;
    public static final String COURSE_1 = FIRST_COURSE;
    public static final String COURSE_2 = THIRD_COURSE;
    public static final int YEAR_1 = 2000;
    public static final int YEAR_2 = 1998;
    public static final int YEAR_3 = 2002;
    public static final int YEAR_4 = 1995;
    public static final List<Student> expected1 = new ArrayList<>(List.of(STUDENT1, STUDENT3));
    public static final List<Student> expected2 = new ArrayList<>(List.of(STUDENT5, STUDENT6));
    public static final List<Student> expected3 = new ArrayList<>(List.of(STUDENT7));
    public static final List<Student> expected4 = new ArrayList<>(List.of(STUDENT3, STUDENT5, STUDENT10));
    public static final List<Student> expected5 = new ArrayList<>(List.of(STUDENT5, STUDENT11));
    public static final List<Student> expected6 = new ArrayList<>(List.of(STUDENT8));
    public static final Student expected7 = STUDENT5;
    public static final Student expected8 = STUDENT9;
    public static final List<String> expected9 = new ArrayList<>(List.of("ln3, fn3", "ln6, fn6", "ln9, fn9"));
    public static final List<String> expected10 = new ArrayList<>(List.of("ln4, fn4", "ln8, fn8", "ln12, fn12"));
}
