package testdata;
import com.homework.model.Student;

import java.util.ArrayList;
import java.util.List;

import static com.homework.constants.FacultyConstants.*;
import static com.homework.constants.GroupConstants.*;
import static com.homework.constants.Students.*;
public class ConditionalUtilTestData {
    public static final List<Student> studentList1 = new ArrayList<>(List.of(STUDENT1, STUDENT3));
    public static final List<Student> studentList2 = new ArrayList<>(List.of(STUDENT2, STUDENT5));
    public static final List<Student> studentList3 = new ArrayList<>(List.of(STUDENT1, STUDENT2));
    public static final List<Student> studentList4 = new ArrayList<>(List.of(STUDENT7, STUDENT11));
    public static final List<Student> studentList5 = new ArrayList<>(List.of(STUDENT2, STUDENT6));
    public static final List<Student> studentList6 = new ArrayList<>(List.of(STUDENT11, STUDENT12));
    public static final List<Student> studentList7 = new ArrayList<>(List.of(STUDENT9, STUDENT10));
    public static final List<Student> studentList8 = new ArrayList<>(List.of(STUDENT9, STUDENT12));
    public static final String FACULTY_1 = IT_FACULTY;
    public static final String FACULTY_2 = CHEMISTRY_FACULTY;
    public static final String FACULTY_3 = IT_FACULTY;
    public static final String FACULTY_4 = ECONOMIC_FACULTY;
    public static final String GROUP_1 = SUPPLY_GROUP;
    public static final String GROUP_2 = TECHNICAL_GROUP;
    public static final String GROUP_3 = SUPPORT_GROUP;
    public static final String GROUP_4 = SUPPLY_GROUP;
    public static boolean ALL_ON_FACULTY = true;
    public static boolean NOT_ALL_ON_FACULTY = false;
    public static boolean ANYONE_ON_FACULTY = true;
    public static boolean NOT_ANYONE_ON_FACULTY = false;
    public static boolean ALL_IN_GROUP = true;
    public static boolean NOT_ALL_IN_GROUP = false;
    public static boolean ANYONE_IN_GROUP = true;
    public static boolean NOT_ANYONE_IN_GROUP = false;
}
