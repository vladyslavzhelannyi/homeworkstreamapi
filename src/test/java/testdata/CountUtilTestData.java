package testdata;

import com.homework.model.Student;
import com.homework.util.student.CountUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

import static com.homework.constants.Students.*;


public class CountUtilTestData {
    public static final List<Student> STUDENT_LIST_1 = new ArrayList<>(List.of(STUDENT1, STUDENT3, STUDENT5));
    public static final List<Student> STUDENT_LIST_2 = new ArrayList<>(List.of(STUDENT5, STUDENT8, STUDENT11));
    public static final int FACULTY_STUDENTS_NUMBER_1 = 2;
    public static final int FACULTY_STUDENTS_NUMBER_2 = 3;

}
