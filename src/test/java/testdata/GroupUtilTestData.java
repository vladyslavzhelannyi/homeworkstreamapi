package testdata;

import com.homework.model.Student;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.homework.constants.CourseConstants.*;
import static com.homework.constants.FacultyConstants.*;
import static com.homework.constants.GroupConstants.*;
import static com.homework.constants.Students.*;

public class GroupUtilTestData {
    public static final List<Student> studentList1 = new ArrayList<>(List.of(STUDENT1, STUDENT3, STUDENT2, STUDENT10));
    public static final List<Student> studentList2 = new ArrayList<>(List.of(STUDENT4, STUDENT7, STUDENT5, STUDENT6));
    public static final List<Student> studentList3 = new ArrayList<>(List.of(STUDENT2, STUDENT11, STUDENT1, STUDENT12));
    public static final List<Student> studentList4 = new ArrayList<>(List.of(STUDENT10, STUDENT3, STUDENT4, STUDENT9));
    public static final List<Student> studentList5 = new ArrayList<>(List.of(STUDENT4, STUDENT5, STUDENT3, STUDENT10));
    public static final List<Student> studentList6 = new ArrayList<>(List.of(STUDENT8, STUDENT11, STUDENT9, STUDENT12));
    public static final List<Student> facultyList1 = new ArrayList<>(List.of(STUDENT1, STUDENT3));
    public static final List<Student> facultyList2 = new ArrayList<>(List.of(STUDENT2, STUDENT10));
    public static final List<Student> facultyList3 = new ArrayList<>(List.of(STUDENT4, STUDENT7));
    public static final List<Student> facultyList4 = new ArrayList<>(List.of(STUDENT5, STUDENT6));
    public static final List<Student> courseList1 = new ArrayList<>(List.of(STUDENT2, STUDENT11));
    public static final List<Student> courseList2 = new ArrayList<>(List.of(STUDENT1, STUDENT12));
    public static final List<Student> courseList3 = new ArrayList<>(List.of(STUDENT10, STUDENT3));
    public static final List<Student> courseList4 = new ArrayList<>(List.of(STUDENT4, STUDENT9));
    public static final List<Student> groupList1 = new ArrayList<>(List.of(STUDENT4, STUDENT5));
    public static final List<Student> groupList2 = new ArrayList<>(List.of(STUDENT3, STUDENT10));
    public static final List<Student> groupList3 = new ArrayList<>(List.of(STUDENT8, STUDENT11));
    public static final List<Student> groupList4 = new ArrayList<>(List.of(STUDENT9, STUDENT12));
    public static final Map<String, List<Student>> expected1 = new HashMap<>();
    public static final Map<String, List<Student>> expected2 = new HashMap<>();
    public static final Map<String, List<Student>> expected3 = new HashMap<>();
    public static final Map<String, List<Student>> expected4 = new HashMap<>();
    public static final Map<String, List<Student>> expected5 = new HashMap<>();
    public static final Map<String, List<Student>> expected6 = new HashMap<>();

    static {
        expected1.put(IT_FACULTY, facultyList1);
        expected1.put(ECONOMIC_FACULTY, facultyList2);
        expected2.put(IT_FACULTY, facultyList3);
        expected2.put(CHEMISTRY_FACULTY, facultyList4);
        expected3.put(FIRST_COURSE, courseList1);
        expected3.put(SECOND_COURSE, courseList2);
        expected4.put(THIRD_COURSE, courseList3);
        expected4.put(FOURTH_COURSE, courseList4);
        expected5.put(TECHNICAL_GROUP, groupList1);
        expected5.put(SUPPORT_GROUP, groupList2);
        expected6.put(SUPPLY_GROUP, groupList3);
        expected6.put(TECHNICAL_GROUP, groupList4);

    }
}
