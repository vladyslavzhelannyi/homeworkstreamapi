package testdata;
import com.homework.model.Student;

import java.util.ArrayList;
import java.util.List;

import static com.homework.constants.AddressConstants.*;
import static com.homework.constants.CourseConstants.*;
import static com.homework.constants.FacultyConstants.*;
import static com.homework.constants.GroupConstants.*;
import static com.homework.constants.Students.*;
public class MapUtilTestData {
    public static final Student newStudent1 = new Student(1, "fn1", "ln1", 1998, LONDON,
            "0951111111", IT_FACULTY, SECOND_COURSE, SUPPORT_GROUP);
    public static final Student newStudent2 = new Student(2, "fn2", "ln2", 1997, TOKYO,
            "0952222222", IT_FACULTY, FIRST_COURSE, SUPPLY_GROUP);
    public static final Student newStudent3 = new Student(3, "fn3", "ln3", 1996, LONDON,
            "0953333333", ECONOMIC_FACULTY, THIRD_COURSE, SUPPORT_GROUP);
    public static final Student newStudent4 = new Student(4, "fn4", "ln4", 1993, TOKYO,
            "0954444444", ECONOMIC_FACULTY, FOURTH_COURSE, TECHNICAL_GROUP);
    public static final Student newStudent5 = new Student(5, "fn5", "ln5", 2003, PARIS,
            "0955555555", CHEMISTRY_FACULTY, THIRD_COURSE, TECHNICAL_GROUP);
    public static final Student newStudent6 = new Student(6, "fn6", "ln6", 1994, PARIS,
            "0956666666", CHEMISTRY_FACULTY, SECOND_COURSE, TECHNICAL_GROUP);
    public static final Student newStudent7 = new Student(7, "fn7", "ln7", 1992, PARIS,
            "0957777777", IT_FACULTY, FIRST_COURSE, SUPPORT_GROUP);
    public static final Student newStudent8 = new Student(8, "fn8", "ln8", 2002, LONDON,
            "0958888888", CHEMISTRY_FACULTY, SECOND_COURSE, SUPPORT_GROUP);
    public static final List<Student> studentList1 = new ArrayList<>(List.of(STUDENT1, STUDENT2));
    public static final List<Student> studentList2 = new ArrayList<>(List.of(STUDENT3, STUDENT4));
    public static final List<Student> studentList3 = new ArrayList<>(List.of(STUDENT5, STUDENT6));
    public static final List<Student> studentList4 = new ArrayList<>(List.of(STUDENT7, STUDENT8));
    public static final String FACULTY_1 = IT_FACULTY;
    public static final String FACULTY_2 = ECONOMIC_FACULTY;
    public static final String GROUP_1 = TECHNICAL_GROUP;
    public static final String GROUP_2 = SUPPORT_GROUP;
    public static final List<Student> expected1 = new ArrayList<>(List.of(newStudent1, newStudent2));
    public static final List<Student> expected2 = new ArrayList<>(List.of(newStudent3, newStudent4));
    public static final List<Student> expected3 = new ArrayList<>(List.of(newStudent5, newStudent6));
    public static final List<Student> expected4 = new ArrayList<>(List.of(newStudent7, newStudent8));
}
